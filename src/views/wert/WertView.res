/* *************************************************************************** */
/*  */
/* Open Source License */
/* Copyright (c) 2019-2021 Nomadic Labs, <contact@nomadic-labs.com> */
/*  */
/* Permission is hereby granted, free of charge, to any person obtaining a */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense, */
/* and/or sell copies of the Software, and to permit persons to whom the */
/* Software is furnished to do so, subject to the following conditions: */
/*  */
/* The above copyright notice and this permission notice shall be included */
/* in all copies or substantial portions of the Software. */
/*  */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER */
/* DEALINGS IN THE SOFTWARE. */
/*  */
/* *************************************************************************** */

open ReactNative

module Form = {
  module View = {
    @react.component
    let make = (~loading, ~account: Alias.t, ~setAccount, ~submit) => <>
      <View style=FormStyles.header>
        <Typography.Overline1> {I18n.Title.wert_recipient->React.string} </Typography.Overline1>
      </View>
      <FormGroupAccountSelector
        label=I18n.Label.send_recipient value=account handleChange={a => setAccount(_ => a)}
      />
      <View style=FormStyles.verticalFormAction>
        <Buttons.SubmitPrimary
          text=I18n.Btn.buy_tez onPress={_ => submit(account.address)} loading
        />
      </View>
    </>
  }
}

type step =
  | Disclaimer
  | SelectRecipient

@react.component
let make = (~account, ~submit, ~closeAction) => {
  let (account, setAccount) = React.useState(() => account)

  let (modalStep, setModalStep) = React.useState(_ =>
    WertDisclaimer.needSigning() ? Disclaimer : SelectRecipient
  )

  let title = switch modalStep {
  | Disclaimer => I18n.Title.notice
  | SelectRecipient => I18n.Title.buy_tez
  }

  let back = None
  let closing = Some(ModalFormView.Close(closeAction))

  <ModalFormView title back ?closing titleStyle=FormStyles.headerMarginBottom8>
    {switch modalStep {
    | Disclaimer =>
      <WertDisclaimerView
        onSign={unsigned =>
          if !unsigned {
            setModalStep(_ => SelectRecipient)
          }}
      />
    | SelectRecipient => <Form.View account setAccount submit loading=false />
    }}
  </ModalFormView>
}
